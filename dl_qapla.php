<?php
/*
  Plugin Name: DL Qapla Plugin
  Description: Plugin per la comunicazione con Qapla.
  Version: 1.0.beta
  Author: Daniele Licitra
  Author URI: danielelicitra.altervista.org
*/

    $qapla_apiversion=1.1;
    $qapla_uri="https://api.qapla.it/".$qapla_apiversion."/";
  
  
    if(!class_exists('DL_Qapla')) {
        
        class DL_Qapla {

            public function __construct(){
                error_log("Avvio modulo",0);
                
                add_action('admin_init', array(&$this, 'admin_init'));
                add_action('admin_menu', array(&$this, 'add_menu'));
                
                //require_once(sprintf("%s/post-types/dlq_tracking.php", dirname(__FILE__)));
                //$dlq_tracking = new DLQ_Tracking();
            }

            public function activate(){}

            public function deactivate(){}

            public function admin_init(){
                $this->init_settings();
            }
            
            
            
            public function init_settings() {
            
                error_log("Inizializzazione impostazioni",0);
                $section_group="dlq";
                $page = $section_group;
                $section_name="dlq_section_api";
                register_setting($section_group, $section_name);
                
                // 1) add sezione -> add_settings_section( $id, $title, $callback, $page )
                // $id - String for use in the 'id' attribute of tags.
                // $title - Title of the section.
                // $callback - Function that fills the section with the desired content. The function should echo its output.
                // $page - The type of settings page on which to show the section (general, reading, writing, media etc.)
                add_settings_section(
                    $section_name, 
                    __( 'API', 'dlq' ),
                    array(&$this, 'dlq_section_api_cb'),
                    $page
                );
                
                // 2) add field
                // doc -> add_settings_field( $id, $title, $callback, $page, $section, $args );
                add_settings_field(
                    'dlq_apikey', 
                    'API key', 
                    array(&$this, 'dlq_settings_print'), 
                    $page,
                    $section_name,
                    array( 
                        'label_for' => 'dlq_apikey',
                        'label' => 'API Key',
                        'idfield' => 'dlq_apikey'
                    )
                );
                
                add_settings_field(
                    'dlq_apikeyprivate', 
                    'API key Privata', 
                    array(&$this, 'dlq_settings_print'), 
                    $page, 
                    $section_name,
                    array( 
                        'label_for' => 'dlq_apikeyprivate',
                        'label' => 'API Key Privata',
                        'idfield' => 'dlq_apikeyprivate'
                    )
                );
                
                // 3) register the settings for this plugin
                register_setting('general', 'dlq_apikey');
                register_setting('general', 'dlq_apikeyprivate');
                error_log("Inizializzazione impostazioni finita",0);                
            } 

            public function add_menu(){
                add_options_page(
                    'DL Qapla Plugin Settings', 
                    'DL Qapla', 
                    'manage_options', 
                    'dlq', 
                    array(&$this, 'plugin_settings_page')
                );
            } 
            
            /**
             * Menu Callback per andare alla pagina dei settings.
             */     
            public function plugin_settings_page(){
                if(!current_user_can('manage_options')){
                    wp_die(__('You do not have sufficient permissions to access this page.'));
                }
                include(sprintf("%s/templates/settings.php", dirname(__FILE__)));
            }

            // section callbacks can accept an $args parameter, which is an array.
            // $args have the following keys defined: title, id, callback.
            // the values are defined at the add_settings_section() function.
            public function dlq_section_api_cb( $args ) {
                error_log("test sezione",0);
                echo __( 'This section description', 'dlq' );
                echo "<div><h2>Impostazione API</h2></div>";
            }
            
            public function dlq_settings_print($args){
                //error_log("test",0);
                $option = get_option( $args['idfield'],'' );
                //var_dump($args);
                //var_dump($option);
                ?>
                <div>
                    <input  type='text' 
                            id='<?php echo $args['idfield']; ?>' 
                            name='<?php echo $args['idfield']; ?>' 
                            value='<?php echo $option; ?>' />
                </div>
                <?php
            }            




            public function queryTracking($tracknumber){
                
                $response = wp_remote_get( 
                    $qapla_uri."getTrack/?"
                    ."apiKey=".get_option( 'dlq_apikeyprivate' )
                    ."&trackingNumber=".$tracknumber
                    ."&lang=ita" 
                );
                if ( is_array( $response ) ) {
                  $header = $response['headers']; // array of http header lines
                  $body = $response['body']; // use the content
                }
            }
        
            
            //https://api.qapla.it/1.1/getCouriers/?apiKey=chiavesegreta
            public function queryCouriers($nation){
                
                $response = wp_remote_get( 
                    $qapla_uri."getCouriers/?apiKey=".get_option( 'dlq_apikeyprivate' )
                );
                if ( is_array( $response ) ) {
                  $header = $response['headers']; // array of http header lines
                  $body = $response['body']; // use the content
                }
            }
        
        
        

        } // end DL_Qapla class
    }



    if(class_exists('DL_Qapla')) {
        register_activation_hook(__FILE__,array('DL_Qapla', 'activate'));
        register_deactivation_hook(__FILE__, array('DL_Qapla', 'deactivate'));
        // instantiate the plugin class
        $qapla = new DL_Qapla();
        
        // Add a link to the settings page onto the plugin page
        if(isset($qapla)){
            // Add the settings link to the plugins page
            function plugin_settings_link($links){ 
                $settings_link = '<a href="options-general.php?page=dlq">Settings</a>'; 
                array_unshift($links, $settings_link); 
                return $links; 
            }
            $plugin = plugin_basename(__FILE__); 
            add_filter("plugin_action_links_$plugin", 'plugin_settings_link');
        }
   }

?>
