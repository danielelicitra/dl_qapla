<div class="wrap">
    <h2>DL Qapla Plugin</h2>
    <div>
        Usa questa pagina per configurare la comunicazione con
        Qapla.
    </div>
    
    <div>
        Le informazioni necessarie sono presenti nella tua area
        privata di Qapla.
    </div>
    
    <form method="post" action="options.php"> 
        <?php 
            @settings_fields('dlq');
            @do_settings_fields('dlq','dlq_section_api');
            @submit_button(); 
        ?>
    </form>
    
</div>
