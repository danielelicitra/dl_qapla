# dl_qapla

Developed by Daniele Licitra

danielelicitra at gmail.com



## Descrizione

Semplice plugin per l'interrogazione delle API di Qapla.
Per maggiori dettagli sulle API Qapla, visita il sito
https://api.qapla.it/

## Attenzione

Attualmente i setting vengono visualizzati ma non salvati. (se inseriti a mano sul DB, visualizzo le opzioni, ma se faccio salva non salva nulla???).

Trovate un modo per richiamare i metodi della classe DL_Qapla dalla pagina o utilizzare la pagina check.php

Una volta fatto funzionare il plugin da una pagina, si possono provare i metodi getCourier e quello per piazzare un trasporto.
